# Social Distancing Tool

 A tool that can potentially detect where each person is in real-time, and return a red dot if the distance between two people is dangerously close.




**Team Members**

- V.Gnapana : 19WH1A05H1 : CSE
- G.Mamatha : 19WH1A0455 : ECE
- V.Anusha : 19WH1A05E5 : CSE
- G.Lakshmi Priya :19WH1A1264 : IT
- M.Sreeja : 19WH1A0588 : CSE
- K.Shivani : 19WH1A0225 : EEE
